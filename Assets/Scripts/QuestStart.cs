﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuestStart : MonoBehaviour
{
    //public Transform weaponCheck;
    //public LayerMask questObjects;
    public float checkRadius=1.5f;

    private string questDialogueScene = "DialogueFix";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && Vector2.Distance(GameObject.FindWithTag("Player").transform.position,gameObject.transform.position) < checkRadius){
        	if(Time.timeScale != 0){
	        	//Debug.Log(gameObject.transform.position+" "+weaponCheck.position);
	         	//string hit = Physics2D.OverlapCircle(weaponCheck.position, checkRadius, questObjects).name;
	        	Debug.Log("Quest interacted");

	        	var parameters = new LoadSceneParameters(LoadSceneMode.Additive);
	        	Scene scene = SceneManager.LoadScene(questDialogueScene, parameters);
	        	Debug.Log("scene "+scene.name+" "+scene.isLoaded);
	        	StartCoroutine(WaitForSceneLoad());
	        	Time.timeScale = 0;
	        }
	        else{
	        	SceneManager.UnloadSceneAsync("DialogueFix");
	        	Time.timeScale = 1;
	        }
        }
    }


    //since apparently having async and waiting for a good result
    //is too hard we just use normal and wait a bit
    IEnumerator WaitForSceneLoad(){
    	//works even with paused timescale
    	yield return new WaitForSecondsRealtime(0.1f);
    	if(!SceneManager.GetSceneByName(questDialogueScene).isLoaded){
    		StartCoroutine(WaitForSceneLoad());
    	}
    	else{
    		Debug.Log("Hey the scene got loaded "+SceneManager.GetSceneByName(questDialogueScene).isLoaded);
    		SceneManager.SetActiveScene(SceneManager.GetSceneByName(questDialogueScene));
    	}
    }
}
