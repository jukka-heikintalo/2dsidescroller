﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadPlayerCheck : MonoBehaviour
{
    public float timeDelay = 1f;
    public GameObject endScreen;

    private int playerCount = 0;

    void Start()
    {
        StartCoroutine(DeadPlayer());
    }

    // Update is called once per frame
    void Update()
    {
        
        
        /*if(GameObject.Find("enemy") < enemyCount){
            Debug.Log("hi");
        }*/
    }

    IEnumerator DeadPlayer() {
        //Debug.Log("Started deadcheck at timestamp : " + Time.time);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject obj in players)
        {
            playerCount += 1;
        }
        if(playerCount <= 0){
            Debug.Log("Game over");
            endScreen.SetActive(true);
            endScreen.transform.GetChild(0).GetComponent<Fade>().FadeIn();
        }
        else{
            playerCount = 0;
            yield return new WaitForSeconds(timeDelay);
            StartCoroutine(DeadPlayer());
        }
    }
}
