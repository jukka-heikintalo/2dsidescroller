﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    public Transform weaponCheck;
    public LayerMask enemyObjects;
    public float checkRadius=0.5f;
    public float health=10f;
    public GameObject blood;
    public bool traceHitbox = false;
    public bool god = false;

    public Animator anim;

 	private GameObject enemy = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    	if(god){
    		health = 10f;
    	}
    	
		if(anim.GetFloat("attack") >= 1) {
			//Debug.Log(weaponCheck.position+" "+checkRadius+"  "+enemyObjects+" "+Physics2D.OverlapCircle(weaponCheck.position, checkRadius, enemyObjects));
    		
			//hitbox tracer
			if(traceHitbox){
    			Debug.Log("weapon position "+weaponCheck.position+" check radius "+checkRadius);
    			GameObject marker = (GameObject)Resources.Load("Prefabs/UiSpriteTest");
    			CircleDraw comp = marker.GetComponent("CircleDraw") as CircleDraw;
    			comp.radius = checkRadius;
    			Instantiate(marker, weaponCheck.position, Quaternion.identity);
    		}

    		string enemyHit = Physics2D.OverlapCircle(weaponCheck.position, checkRadius, enemyObjects).name;
        	Debug.Log("HIT "+enemyHit);

			//don't hit yourself
			if(gameObject.name != enemyHit){
				enemy = GameObject.Find(enemyHit);
			}

			if(!enemy){
				Debug.Log("ENEMY NULL");
			}
		}

		//enemy was hit
		if(enemy != null){
			Debug.Log("ENEMY "+enemy.ToString());
			enemy.GetComponent<PlayerAttack>().health -= 1;
			Instantiate(blood, transform.position, Quaternion.identity);

			//Debug.Log(gameObject.name+" Health "+health);
			if(enemy.GetComponent<PlayerAttack>().health <= 0){
				Debug.Log(enemy+" Dead");
				Destroy(enemy);
			}
			enemy = null;
		}
    }
}
