﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChoiceButton : MonoBehaviour
{
    public string option;
    public DialogueManager box;

    public void SetText(string newText) {
        this.GetComponentInChildren<Text> ().text = newText;
    }

    public void SetOption(string newOption) {
        this.option = newOption;
    }

    public void ParseOption() {
        string command = option.Split (',') [0];
        string commandModifier = option.Split (',') [1];
        box.playerTalking = false;
        if (command == "line") {
            box.ShowDialogue(int.Parse(commandModifier));
        } else if (command == "scene") {
            SceneManager.LoadScene("Scene" + commandModifier);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
