﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueManager : MonoBehaviour
{
    DialogueParser parser;

    public string dialogue, characterName;
    public int lineNum;
    int pose;
    string position;
    string modelType;
    string[] options;
    public bool playerTalking;
    List<Button> buttons = new List<Button> ();
    public float spriteOffset = 5f;

    public Text dialogueBox;
    public Text nameBox;
    public GameObject choiceBox;
    public GameObject[] actors;
    public float actorScale = 50f;

    // Start is called before the first frame update
    void Start()
    {
        dialogue = "";
        characterName = "";
        pose = 0;
        position = "L";
        modelType = "sprite";
        playerTalking = false;
        parser = GameObject.Find("DialogueParser").GetComponent<DialogueParser>();
        lineNum = 0;

        //clear sprites on start
        foreach ( GameObject actor in actors){
          SpriteRenderer spriteRen = actor.GetComponent<SpriteRenderer>();
          spriteRen.sprite = null;
        }
        ToggleGameObject("off", "l2d");
        ToggleGameObject("off", "DialogueBox");

        //needed one more click to start the dialogue
        if(playerTalking == false){
          ShowDialogue(lineNum);
        }
    }

    void DeactivateChildren(GameObject g, bool a) 
    {
        g.SetActive(a);
        foreach (Transform child in g.transform) 
        {
            DeactivateChildren(child.gameObject, a);
        }
    }

    void ToggleGameObject(string todo, string what){
      GameObject gObj = GameObject.Find(what);
      if(todo == "on"){
        Debug.Log("We in here");
        DeactivateChildren(gObj, true);
        //DeactivateChildren(GameObject.Find("DialogueBox"), true);
      }
      else if(todo == "off"){
        foreach (Transform child in gObj.transform) 
        {
            DeactivateChildren(child.gameObject, false);
        }
      }
      else{
        if(gObj.transform.GetChild(0).gameObject.activeSelf == true){
          //deactivate children but not itself
          foreach (Transform child in gObj.transform) 
          {
              DeactivateChildren(child.gameObject, false);
          }
        }
        else{
          DeactivateChildren(gObj, true);
        }
      }
    }

    // Update is called once per frame
    void Update()
    {
      if (Input.GetMouseButtonDown(0)) {
        Debug.Log(
          "dialogue = "+dialogue+
          " characterName = "+characterName+
          " pose = "+pose+
          " position = "+position+
          " playerTalking = "+playerTalking
        );


        //general dialogue progression
        //choice paths (linenum) get set by buttons in choicebuttons
        if(playerTalking == false){
          ShowDialogue(lineNum);
        }
      }
    }

    public void ShowDialogue(int line) {
      lineNum = line;
      ToggleGameObject("on", "DialogueBox");
      try
      {
        ResetImages(lineNum);
      }
      catch{}
      ParseLine(lineNum);
      lineNum++;
    }

    void ResetImages(int line) {
        if (characterName != "") {
          if(modelType == "sprite"){
            GameObject character = GameObject.Find (characterName);
            SpriteRenderer currSprite = character.GetComponent<SpriteRenderer>();
            currSprite.sprite = null;
          }
          else{
            ToggleGameObject("off","l2d");
          }
        }
    }

    void ParseLine(int line) {
      Debug.Log("Hello+"+line);

      //self destruct if ran out of lines
      if(parser.GetName (line) != "Player" && parser.GetContent (line) == ""){
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
      }

      if (parser.GetName (line) != "Player") {
          ClearButtons();
          playerTalking = false;
          nameBox.text = characterName = parser.GetName (line);
          dialogueBox.text = dialogue = parser.GetContent (line);
          position = parser.GetPosition (line);
          modelType = parser.GetModel(line);
        if(modelType == "sprite"){
          pose = parser.GetPose (line);
          DisplayImages();
        }
        else if(modelType == "l2d"){
          ToggleGameObject("on", "l2d");
        }
      /*ONLY PLAYER TALKING*/
      } else {
          ToggleGameObject("off", "DialogueBox");
          playerTalking = true;
          characterName = "";
          dialogue = "";
          pose = 0;
          position = "";
          modelType = "player";
          options = parser.GetOptions(line);
          CreateButtons();
      }
    }

    void DisplayImages() {
        if (characterName != "") {
          GameObject character = GameObject.Find(characterName);
          //position on screen L/R
          SetPosition(character);
          //set character pose if one exists
          if(modelType == "sprite"){
            SpriteRenderer currSprite = character.GetComponent<SpriteRenderer>();
            currSprite.sprite = character.GetComponent<Character>().characterPoses[pose];
          }
        }
    }

    void SetPosition(GameObject spriteObj) {
        if (position == "L") {
            spriteObj.transform.position = new Vector3(gameObject.transform.position.x - spriteOffset, gameObject.transform.position.y, gameObject.transform.position.z);
            spriteObj.transform.localScale = new Vector3(actorScale, actorScale, actorScale);
        } else if (position == "R") {
            spriteObj.transform.position = new Vector3(gameObject.transform.position.x + spriteOffset, gameObject.transform.position.y, gameObject.transform.position.z);
            spriteObj.transform.localScale = new Vector3(actorScale, actorScale, actorScale);
        }
    }

    void CreateButtons() {
        for (int i = 0; i < options.Length; i++) {
            GameObject button = (GameObject)Instantiate(choiceBox);
            Button b = button.GetComponent<Button>();
            ChoiceButton cb = button.GetComponent<ChoiceButton>();
            cb.SetText(options[i].Split(':')[0]);
            cb.option = options[i].Split(':')[1];
            cb.box = this;
            b.transform.SetParent(this.transform);
            b.transform.localPosition = new Vector3(0,-25 + (i*50));
            b.transform.localScale = new Vector3(1, 1, 1);
            buttons.Add (b);
        }
    }

    void ClearButtons() {
      while (buttons.Count > 0){
          print ("Clearing buttons "+buttons.Count);
          Button b = buttons[0];
          buttons.Remove(b);
          Destroy(b.gameObject);
      }
    }
}
