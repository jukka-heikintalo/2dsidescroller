﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public GameObject player;
    public float leftOffset = 5f;
    public bool originalOffset = false;
    private Vector3 fixedCam;


    void Start()
    {
        if (player == null){
            player = GameObject.FindWithTag("Player");
        }
        if(originalOffset){
            leftOffset = gameObject.transform.position.x - player.transform.position.x;
        }
    }

    void LateUpdate()
    {
        //keeps the camera on the player
        gameObject.transform.position = new Vector3(player.transform.position.x + leftOffset, transform.position.y, transform.position.z);
    }
}