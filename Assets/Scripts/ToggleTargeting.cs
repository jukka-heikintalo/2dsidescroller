﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTargeting : MonoBehaviour
{
    void DeactivateChildren(GameObject g, bool a) 
    {
        g.SetActive(a);
        
        foreach (Transform child in g.transform) 
        {
            DeactivateChildren(child.gameObject, a);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F2))
        {
          if(gameObject.transform.GetChild(0).gameObject.activeSelf == true){
            //deactivate children but not itself
            foreach (Transform child in gameObject.transform) 
            {
                DeactivateChildren(child.gameObject, false);
            }
          }
          else{
            DeactivateChildren(gameObject, true);
          }
        }
    }
}
