﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class quit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Quit is alive!");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
        
        //pause
        if(Input.GetKeyDown(KeyCode.P)){
            if(Time.timeScale != 0){
                Debug.Log("Paused "+Time.timeScale);
                Time.timeScale = 0;
            }
            else{
                Debug.Log("Unpaused "+Time.timeScale);
                Time.timeScale = 1;
            }
        }
    }
}
