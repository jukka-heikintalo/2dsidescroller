﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	public GameObject enemy;
    public GameObject[] models;
	public float timeDelay = 5f;
    public float maxDelay = 5f;
	public float enemyCount = 3f;

	private float currentEnemyCount = 0f;
    // Start is called before the first frame update
    void Start()
    {
    	StartCoroutine(SpawnEnemy());
    }

    // Update is called once per frame
    void Update()
    {
    	
    	
    	/*if(GameObject.Find("enemy") < enemyCount){
    		Debug.Log("hi");
    	}*/
    }

	IEnumerator SpawnEnemy() {
		//Debug.Log("Started enemyspawn at timestamp : " + Time.time);
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        //Debug.Log("modelsefasdfa "+models.Length+" range "+Random.Range(0, models.Length));
        if(models.Length > 0){
            //int random is exlusive and only goes to length-1
            int random = Random.Range(0, models.Length);
            Instantiate(models[random], transform.position, Quaternion.identity);
        }
        else{
    		foreach (GameObject enemyL in enemies)
            {
                //only count enemies with same name
                //so if you want multiple spawn rename new spawns uniquely
                if(enemyL.name == (enemy.name+"(Clone)") || enemyL.name == enemy.name){
                    currentEnemyCount += 1;
                }
            }

            if(currentEnemyCount < enemyCount){
            	Instantiate(enemy, transform.position, Quaternion.identity);	    
            }
            currentEnemyCount = 0;
        }

        //change random seed inside loop
        //Random.InitState((int)Time.time);
        yield return new WaitForSeconds(Random.Range(timeDelay, maxDelay));
	    StartCoroutine(SpawnEnemy());
	}
}