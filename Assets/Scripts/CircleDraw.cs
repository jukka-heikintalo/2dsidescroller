﻿using UnityEngine;
using System.Collections;

public class CircleDraw : MonoBehaviour
{
    public float ThetaScale = 0.01f;
    public float radius = 0.5f;
    public float width = 0.01f;
    public bool update = false;
    private int Size;
    private LineRenderer LineDrawer;
    private float Theta = 0f;

    void Start ()
    {       
        LineDrawer = gameObject.AddComponent<LineRenderer>();
        //LineDrawer.material = new Material(Shader.Find("Particles/Additive"));
        LineDrawer.SetColors(new Color(0, 1, 1, 1), new Color(1, 0, 0, 1));
        LineDrawer.SetWidth(width, width);

        Theta = 0f;
        Size = (int)((1f / ThetaScale) + 1f);
        LineDrawer.SetVertexCount(Size); 
        for(int i = 0; i < Size; i++){          
            Theta += (2.0f * Mathf.PI * ThetaScale);         
            float x = radius * Mathf.Cos(Theta);
            float y = radius * Mathf.Sin(Theta);
            x += gameObject.transform.position.x;
            y += gameObject.transform.position.y;
            LineDrawer.SetPosition(i, new Vector3(x, y, 0));
        }
    }

    void Update ()
    {      
        if(update){
            Theta = 0f;
            Size = (int)((1f / ThetaScale) + 1f);
            LineDrawer.SetVertexCount(Size); 
            for(int i = 0; i < Size; i++){          
                Theta += (2.0f * Mathf.PI * ThetaScale);         
                float x = radius * Mathf.Cos(Theta);
                float y = radius * Mathf.Sin(Theta);
                x += gameObject.transform.position.x;
                y += gameObject.transform.position.y;
                LineDrawer.SetPosition(i, new Vector3(x, y, 0));
            }
        }
    }
}