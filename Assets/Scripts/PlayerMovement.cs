﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;
    public Transform ceilingCheck;
    public Transform groundCheck;
    public LayerMask groundObjects;
    public float checkRadius;
    public int maxJumpCount;
    public bool startsLookingRight = true;
    public float attackDistance = 1;
    public float attackDelay = 0.5f;

    public Animator anim;

    private Rigidbody2D rb;
    private bool facingRight = true;
    private float moveDirection;
    private bool isJumping = false;
    private bool isGrounded;
    private int jumpCount;

    private Vector2 target;
    private Vector2 position;
    private bool canAttack = true;

    private void Awake(){
        rb = GetComponent<Rigidbody2D>(); //looks for a component in this game object
    }

    // Start is called before the first frame update
    void Start()
    {
    	//always begins with looking right
    	if(startsLookingRight == false){
    		FlipCharacter();
    	}
        jumpCount = maxJumpCount;
    }

    // Update is called once per frame
    void Update()
    {
        //get inputs
        ProcessInputs();
        
        //animate
        Animate();
    }

    IEnumerator AttackRoutine(){
        canAttack = false;
        anim.SetFloat("attack",0.5f);
        yield return new WaitForSeconds(0.1f);
        anim.SetFloat("attack",1);
        Debug.Log("attack");
        yield return new WaitForSeconds(0.1f);
        anim.SetFloat("attack",0);
        yield return new WaitForSeconds(attackDelay);
        canAttack = true;
    }

    //better for handling physics, can be called multiple times per update frame
    //physics in fixedupdate
    private void FixedUpdate(){
        //check if grounded
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, groundObjects);
        if(isGrounded){
            jumpCount = maxJumpCount;
        }

        //move
        Move();
    }

    private void ProcessInputs(){
    	if(gameObject.tag == "Player"){
            if(Input.GetAxis("Fire1") > 0 && canAttack){
                StartCoroutine(AttackRoutine());
            }

	        moveDirection = Input.GetAxis("Horizontal"); //scale of -1 -> 1
	        if(Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.W)){
	            isJumping = true;
	        }
	    }
	    else if(gameObject.tag == "Enemy"){
	    	target = new Vector2(GameObject.FindWithTag("Player").transform.position.x,GameObject.FindWithTag("Player").transform.position.y);
	    	position = gameObject.transform.position;

		    if(Vector2.Distance(target,position) < attackDistance && canAttack){
                StartCoroutine(AttackRoutine());
		    	moveDirection = 0;
		    }
	    	else if(Vector2.Distance(target,position) < 7){
	    		//check if paused
	    		if(Time.timeScale != 0){
		    		transform.position = Vector2.MoveTowards(transform.position, target, moveSpeed);

			    	float enemyX = gameObject.transform.position.x;
			    	float targetX = target.x;
			    	moveDirection = targetX - enemyX;
			    }
		    	//Debug.Log("ahh"+Vector2.Distance(target,position));
		    }
		    else{
		    	moveDirection = 0;
		    }

	    	//Debug.Log(gameObject.name+" here I am "+target+" "+position+" "+transform.position+ " "+moveDirection);
	    }
    }

    private void Animate(){
        if(moveDirection > 0 && !facingRight){
            FlipCharacter();
        }
        else if(moveDirection < 0 && facingRight){
            FlipCharacter();
        }
    }

    private void Move(){
    	if(gameObject.tag == "Player"){
	        rb.velocity = new Vector2(moveDirection * moveSpeed, rb.velocity.y);
	    }
        anim.SetFloat("horizontal",moveDirection);
        if(isJumping && jumpCount > 0/*isGrounded*/){
            Debug.Log("jumping "+isJumping+" jumps left "+jumpCount);
            rb.AddForce(new Vector2(0f,jumpForce));
            jumpCount--;
        }
        isJumping = false;
    }

    private void FlipCharacter(){
        facingRight = !facingRight; //inverse boolean
        transform.Rotate(0f,180f,0f);
    }
}
