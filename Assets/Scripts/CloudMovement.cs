﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMovement : MonoBehaviour
{
    public GameObject cloud;
    public float speed = 0.1f;
    public bool randomizeSpeed = true;

    // Start is called before the first frame update
    void Start()
    {
        if(cloud == null){
            cloud = gameObject;
        }
        if(randomizeSpeed){
            speed = Random.Range(0.008f, 0.005f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        cloud.transform.position = new Vector2((cloud.transform.position.x - speed), cloud.transform.position.y);
    }
}
