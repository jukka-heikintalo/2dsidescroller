﻿using UnityEngine;

public class ToggleTargetColor : MonoBehaviour
{
    // Fade the color from red to green
    // back and forth over the defined duration

    Color colorStart = Color.red;
    Color colorEnd = Color.green;
    float duration = 1.0f;
    Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer> ();
        rend.material.color = new Color(1f,1f,1f,0f);
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F1))
        {
          if(rend.material.color == new Color(1f,1f,1f,0f)){
            rend.material.color = new Color(1f,1f,1f,1f);
          }
          else{
            rend.material.color = new Color(1f,1f,1f,0f); //invisible
          }
        }
    }
}