﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    public GameObject target = null;
    public bool fadeOutOnstart = false;
    public float speedup = 0;

    void Start()
    {
        if(fadeOutOnstart){
            Debug.Log(target.name+" fadeout "+fadeOutOnstart);
            FadeOutDisable();
        }
    }

    public void FadeOut(){
        StartCoroutine(FadeImage(true,true));
    }

    public void FadeOutDisable(){
        StartCoroutine(FadeImage(true,false));
    }

    public void FadeIn(){
        StartCoroutine(FadeImage(false,true));
    }

    IEnumerator FadeImage(bool fadeAway, bool active)
    {
        if(target == null){
            target = gameObject;
        }
        target.SetActive(true);

        Debug.Log("Fadeout "+fadeAway);
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= (Time.deltaTime + speedup))
            {
                // set color with i as alpha
                target.GetComponent<SpriteRenderer>().material.color = new Color(1, 1, 1, i);
                yield return null;
            }
            target.SetActive(active);
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += (Time.deltaTime + speedup))
            {
                // set color with i as alpha
                target.GetComponent<SpriteRenderer>().material.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }
}
